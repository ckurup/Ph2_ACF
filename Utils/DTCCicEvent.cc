/*

        FileName :                     Event.cc
        Content :                      Event handling from DAQ
        Programmer :                   Nicolas PIERRE
        Version :                      1.0
        Date of creation :             10/07/14
        Support :                      mail to : nicolas.pierre@icloud.com

 */

#include "../Utils/DTCCicEvent.h"
#include "../HWDescription/BeBoard.h"
#include "../HWDescription/Definition.h"
#include "../HWDescription/OuterTrackerHybrid.h"
#include "../Utils/ChannelGroupHandler.h"
#include "../Utils/DataContainer.h"
#include "../Utils/EmptyContainer.h"
#include "../Utils/Occupancy.h"

using namespace Ph2_HwDescription;

const unsigned N2SHYBRIDS = 12;

namespace Ph2_HwInterface
{
// Event implementation
DTCCicEvent::DTCCicEvent(const BeBoard* pBoard, const std::vector<uint32_t>& list, bool pWith8CBC3, bool pWithTLU)
{
    fIsSparsified = pBoard->getSparsification();
    if(fIsSparsified) 
    {
        LOG(INFO) << BOLDRED << "DTCCicEvent::DTCCicEvent - sparsified data for 2S modules not implemented yet" << RESET;
        throw Exception("DTCCicEvent::DTCCicEvent - sparsified data for 2S modules not implemented yet");
    }
    fEventHitList.clear();
    fEventStubList.clear();
    fEventRawList.clear();
    fHybridIds.clear();
    fChipIds.clear();
    fNCbc       = 0;
    fIs8CBC3    = pWith8CBC3;
    fTLUenabled = (uint8_t)pWithTLU;
    // assuming that HybridIds aren't shared between links
    fIs2S = false;
    int hybridNumber = 0;
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            hybridNumber++;
            auto  cOuterTrackerHybrid = static_cast<OuterTrackerHybrid*>(cHybrid);
            auto& cCic                = cOuterTrackerHybrid->fCic;
            fNCbc += (cCic == NULL) ? cHybrid->fullSize() : 1;
            HybridData cHybridData;
            fEventStubList.push_back(cHybridData);
            fHybridIds.push_back(cHybrid->getId());
            fHybridIdsCic.push_back(cHybrid->getId());

            std::vector<uint8_t> cChipIds(0);
            cChipIds.clear();
            for(auto cChip: *cHybrid)
            {
                // only count MPAs
                if(cChip->getFrontEndType() != FrontEndType::CBC3) {
                    throw Exception("DTCCicEvent::DTCCicEvent - only 2S modules (aka just CBC3) chips are supported");
                }
                //if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) continue; // TODO: not sure what this line is counting but othewrwise we get only two chips
                cChipIds.push_back(cChip->getId() % 8);
                fIs2S = fIs2S || cChip->getFrontEndType() == FrontEndType::CBC3;
            }
            fChipIds.push_back(cChipIds);
            fNStripClusters.push_back(0);
            fNPxlClusters.push_back(0);
            if(fIsSparsified) { fEventHitList.push_back(cHybridData); }
            else
            {
                RawHybridData cRawHybridData;
                fEventRawList.push_back(cRawHybridData);
            }
        } // hybrids
    }     // opticalGroup
    fBeId        = pBoard->getId();
    fBeFWType    = 0;
    fCBCDataType = 0;
    fBeStatus    = 0;

    fFeMapping = (fIs2S) ? fFeMapping2S : fFeMappingPSR;

    this->Set(pBoard, list);
}

void DTCCicEvent::Set(const BeBoard* pBoard, const std::vector<uint32_t>& pData)
{
    //const uint16_t LENGTH_CIC_HEADER     = 1; //cic header = | 14bits = 0 | 9 bits = errorBits | 9 bits = L1ID | --> 1 32 bits word
    const uint16_t LENGTH_DAQPATH_HEADER = 1;
    
    const uint32_t VALID_CIC2_HEADER  = 0x0000;//0x3FFF;//valid event header is 14msb = 0 
    const uint32_t VALID_CIC1_HEADER  = 0x55555555;
    const uint32_t PICK_9_BITS       = 0x01FF;

    //unsigned fEventID = (*pData.begin() >> 16) & 0x0000FFFF;

    std::vector<uint32_t> cEventData(pData.begin(), pData.end());
    //print in hex cEventData
    auto cEventIterator = cEventData.begin();

    //int cCicHeaderCounter     =  0;
    int cDaqPathHeaderCounter =  0;
    int cChannelID = 0;
    cChannelID--;
    int wordCounter = 0;
    do {        
        uint32_t cEventId = static_cast<uint32_t> ((*cEventIterator)>> 16);//eventID
        LOG(DEBUG) << "Event # " << cEventId << RESET;
        //uint32_t cNWords  = static_cast<uint32_t> ((*cEventIterator) & 0xFFFF);//number of words in this event
        //ATM skipping IDNW word - you can get the number of words from daqpath from this words - could be useful to check if the event is complete but management between the two types of readout must be handled
        cEventIterator++; //IDNW Word
        wordCounter++;

        for(auto cOpticalGroup: *pBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto   cHybridIndex        = getHybridIndex(cHybrid->getId());
                auto   cOuterTrackerHybrid = static_cast<OuterTrackerHybrid*>(cHybrid);
                auto&  cCic                = cOuterTrackerHybrid->fCic;
                bool   cWithCIC2           = (cCic->getFrontEndType() == FrontEndType::CIC2);
                size_t cNReadoutChips      = (cCic == NULL) ? cHybrid->fullSize() : 1;
                if(!fIsSparsified) cNReadoutChips = 8; //always getting info from all cbcs in unsparsified data
                if(cDaqPathHeaderCounter < LENGTH_DAQPATH_HEADER && cEventIterator < cEventData.end()) {
                    // DAQPATH EVT HEADER:
                    // ffff -> header
                    // 8b ch number 
                    // 8b #32b words
                    if((*cEventIterator >> 16) == 0xFFFF)
                    {
                        cDaqPathHeaderCounter++;
                        cChannelID = static_cast<int>((*cEventIterator) >> 8 & 0xFF);
                    }
                    cEventIterator++;
                    wordCounter++;
                }
                if(cWithCIC2) 
                {
                    while((*cEventIterator & 0x3FFF) != VALID_CIC2_HEADER && cEventIterator < cEventData.end()) {
                        wordCounter++;
                        cEventIterator++;
                    }
                    if((*cEventIterator & 0x3FFF) == VALID_CIC2_HEADER) {
                        // header CIC2: L1A - eventid (9b) - error (9b) - 0 (14b) 
                        fErrorBits = (*cEventIterator >> 14) & PICK_9_BITS;
                        fL1Id      = (*cEventIterator >> 23) & PICK_9_BITS;
                        wordCounter++;
                        cEventIterator++;
                    }
                }
                else{
                    while((*cEventIterator != VALID_CIC1_HEADER) && cEventIterator < cEventData.end()) {
                        wordCounter++;
                        cEventIterator++;
                    }
                    if((*cEventIterator = VALID_CIC1_HEADER)) {
                        wordCounter++;
                        cEventIterator++;
                    }
                }

                std::pair<uint16_t, uint16_t> cL1Information(fErrorBits, fL1Id);
                fEventRawList[cHybridIndex].first = cL1Information;
                fEventRawList[cHybridIndex].second.clear();
                fEventRawList[cHybridIndex].second.resize(cNReadoutChips);

                if(cWithCIC2)
                {
                    // 275 bits per chip ... 8chips... blocks of 11 bits = 200 blocks
                    const size_t cNblocks = RAW_L1_CBC * cNReadoutChips /L1_BLOCK_SIZE; 
                    std::vector<std::bitset<L1_BLOCK_SIZE>> cL1Words(cNblocks, 0);//vector of 11 bit words

                    // split 32 bit words in  blocks of 11 bits
                    int endEventIt = RAW_L1_CBC * cNReadoutChips / 32 + 1;
                    
                    if(cEventIterator + endEventIt > cEventData.end()) {
                        throw Exception("DTCCicEvent::Set: event iterator out of range");
                    }
                    
                    std::vector<uint32_t> cCICData(cEventIterator , cEventIterator + endEventIt);
                    this->splitStream(cCICData, cL1Words, 0, cNblocks);

                    //loop over the number of CBCs... NOTA BENE: cChipIndex = 0 in this loop corresponds to CBC7
                    for(size_t cChipIndex = 0; cChipIndex < cNReadoutChips; cChipIndex++) {
                        std::bitset<RAW_L1_CBC> cBitset(0);
                        size_t cPosition = 0;

                        //loop over the blocks: 275/11 = 25 blocks/CBC
                        for(size_t cBlockIndex = 0; cBlockIndex < RAW_L1_CBC /L1_BLOCK_SIZE; cBlockIndex++) {
                            auto  cIndex   = cChipIndex + cNReadoutChips * cBlockIndex;//CBCId + 8*nBlock
                            auto& cL1block = cL1Words[cIndex];//select the cIndex L1Word (the type is bitset<11>) 
                            for(size_t cNbit = 0; cNbit < cL1block.size(); cNbit++) {
                                cBitset[cBitset.size() - 1 - cPosition] = cL1block[cL1block.size() - 1 - cNbit];
                                cPosition++;
                            }
                        }
                        LOG(DEBUG) << BOLDBLUE << "\t...  chip        : " << std::dec << cChipIndex << "     \t -- " << std::hex << std::bitset<RAW_L1_CBC>(cBitset) << std::dec << RESET;
                        fEventRawList[cHybridIndex].second[cNReadoutChips - 1 - cChipIndex] = cBitset;
                        
                    } // CBC loop
                    //^^^ Since cChipIndex = 0 here corresponds to CBC7, I am filling the vector in descending order ^^^
                    // 275bit/CBC*8CBC = 2200 bit ---> 2200/32 = 68.75 ---> a RawL1Word is composed of 69 words
                }
                else
                {
                    const size_t                     cNblocks = cNReadoutChips; // 274 bits per chip ..
                    const size_t                     cRawL1   = RAW_L1_CBC - 1;
                    std::vector<std::bitset<cRawL1>> cL1Words(cNblocks, 0);
                    int endEventIt = cRawL1 * cNReadoutChips / 32 + 1;
                    if(cEventIterator + endEventIt > cEventData.end()) {
                        LOG(INFO) << BOLDRED << "Error: event iterator out of range" << RESET;
                    }
                    std::vector<uint32_t> cCICData(cEventIterator, cEventIterator + endEventIt);
                    
                    this->splitStream(cCICData, cL1Words, 0, cNblocks); // split 32 bit words in  blocks of 274 bits.

                    for(size_t cChipIndex = 0; cChipIndex < cNReadoutChips; cChipIndex++)
                    {
                        fEventRawList[cHybridIndex].second[cNReadoutChips - 1 - cChipIndex] = (std::bitset<RAW_L1_CBC>((cL1Words[cChipIndex]).to_string() + "0"));
                        LOG(DEBUG) << BOLDBLUE << "\t...  chip " << +cChipIndex << "\t -- " << fEventRawList[cHybridIndex].second[cNReadoutChips - 1 - cChipIndex] << RESET;
                    }

                }
                cEventIterator += (RAW_L1_CBC * cNReadoutChips / 32 + 1);
                wordCounter += (RAW_L1_CBC * cNReadoutChips / 32 + 1);
                cDaqPathHeaderCounter = 0;
            }
        }
    } while(cEventIterator < cEventData.end());
return ;

}

void DTCCicEvent::fillChipDataContainer(ChipDataContainer* chipContainer, const std::shared_ptr<ChannelGroupBase> testChannelGroup, uint16_t hybridId)
{
    std::vector<uint32_t> cHits = this->GetHits(hybridId, chipContainer->getId());
    for(auto cHit: cHits)
    {
        if(testChannelGroup == nullptr) throw Exception("DTCCicEvent::fillChipDataContainer: testChannelGroup is nullptr");
        if(testChannelGroup->isChannelEnabled(cHit))
        {
            chipContainer->getChannelContainer<Occupancy>()->at(cHit).fOccupancy += 1.;
        }
    }
}

void DTCCicEvent::SetEvent(const BeBoard* pBoard, uint32_t pNbCbc, const std::vector<uint32_t>& list)
{
    LOG(INFO) << BOLDRED << "DTCCicEvent::SetEvent not implemented" << RESET;
    throw Exception("DTCCicEvent::SetEvent not implemented");
}

uint8_t DTCCicEvent::GetNStripClusters(uint8_t pHybridId) const
{
    LOG(INFO) << "DTCCicEvent::GetNStripClusters - sparsified data for 2S modules not implemented yet" << RESET;
    auto cHybridIndex = getHybridIndex(pHybridId);
    if(cHybridIndex >= fNStripClusters.size()) 
    { 
        LOG(INFO) << BOLDRED << " DTCCicEvent::GetNStripClusters out of range..." << RESET;
        throw Exception("DTCCicEvent::GetNStripClusters out of range...");
    }
    return fNStripClusters[cHybridIndex];
}

uint8_t DTCCicEvent::GetNPixelClusters(uint8_t pHybridId) const
{
    LOG(INFO) << "DTCCicEvent::GetNPixelClusters - pixel related functions have not been tested yet" << RESET;
    auto cHybridIndex = getHybridIndex(pHybridId);
    if(cHybridIndex >= fNStripClusters.size()) {
        LOG(INFO) << BOLDRED << " DTCCicEvent::GetNPixelClusters out of range..." << RESET;
        throw Exception("DTCCicEvent::GetNPixelClusters out of range...");
    }
    return fNPxlClusters[cHybridIndex];
}

std::vector<PCluster> DTCCicEvent::GetPixelClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    LOG(INFO) << "DTCCicEvent::GetPixelClusters - pixel related functions have not been tested yet" << RESET;
    std::vector<PCluster> cPClusters;
    auto                  cHybridIndex = getHybridIndex(pHybridId);
    if(cHybridIndex >= fEventHitList.size())
    {
        LOG(INFO) << BOLDRED << " DTCCicEvent::GetPixelClusters out of range..." << RESET;
        return cPClusters;
    }
    auto cClusterWords = fEventHitList[cHybridIndex].second;
    if(cClusterWords.size() == 0) return cPClusters;

    // LOG (INFO) << BOLDGREEN << " DTCCicEvent::GetPixelClusters for Fe" << +pHybridId << "..." << RESET;
    auto cIterator = cClusterWords.begin() + GetNStripClusters(pHybridId);
    auto cEnd      = cClusterWords.end();
    while(cIterator < cEnd)
    {
        uint32_t cVal   = static_cast<uint32_t>((*cIterator));
        uint32_t cId    = (uint32_t)((cVal & (0x7 << 14)) >> 14);
        uint32_t cAdd   = (uint32_t)((cVal & (0x7F << 7)) >> 7); //( cL1Word.to_ulong() & (0x7F << 4)) << 4;
        uint32_t cWdth  = (uint32_t)((cVal & (0x7 << 4)) >> 4);  //( cL1Word.to_ulong() & (0x7 << 1)) << 1;
        uint32_t cZInfo = (uint32_t)((cVal & (0xF << 0)) >> 0);  //( cL1Word.to_ulong() & (0x1 << 0)) << 0;

        uint8_t cChipId       = cId; //((*cIterator) & ((0x7) << (0 + 4 + 3 + 7))) >> (0 + 4 + 3 + 7);
        auto    cChipIdMapped = this->getChipIdMapped(pHybridId, pReadoutChipId);
        // LOG(INFO) << BOLDBLUE << "Retreiving pixel information for Hybrid#" << +pHybridId << " Chip#" << +cChipId << " this is chip Id #" << +cChipIdMapped << " in CIC land" << RESET;
        if(cChipId == cChipIdMapped)
        {
            PCluster aPCluster;

            // LOG (INFO) << BOLDGREEN << "PCLUS ..... " << std::bitset<16>(*cIterator)  << RESET;
            aPCluster.fAddress = cAdd;   //((*cIterator) & ((0x7F) << (0 + 4 + 3))) >> (0 + 4 + 3);
            aPCluster.fWidth   = cWdth;  //((*cIterator) & ((0x7) << (0 + 4))) >> (0 + 4);
            aPCluster.fZpos    = cZInfo; //((*cIterator) & ((0xF) << 0)) >> 0;
            cPClusters.push_back(aPCluster);
            // LOG(INFO) << BOLDGREEN << "P-cluster in chip " << +pReadoutChipId << ", address : " << unsigned(aPCluster.fAddress) << "," << unsigned(aPCluster.fWidth) << ","
            //            << unsigned(aPCluster.fZpos) << RESET;
        }
        cIterator++;
    }

    return cPClusters;
}

std::vector<SCluster> DTCCicEvent::GetStripClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    LOG(INFO) << "DTCCicEvent::GetStripClusters - sparsified data for 2S modules not implemented yet" << RESET;
    std::vector<SCluster> cSClusters;
    auto                  cHybridIndex = getHybridIndex(pHybridId);
    if(cHybridIndex >= fEventHitList.size())
    {
        LOG(INFO) << BOLDRED << " DTCCicEvent::GetPixelClusters out of range..." << RESET;
        return cSClusters;
    }
    auto cClusterWords = fEventHitList[getHybridIndex(pHybridId)].second;
    if(cClusterWords.size() == 0) return cSClusters;

    auto cIterator = cClusterWords.begin();
    auto cEnd      = cClusterWords.begin() + GetNStripClusters(pHybridId);
    while(cIterator < cEnd)
    {
        uint32_t cVal  = static_cast<uint32_t>((*cIterator));
        uint16_t cId   = (uint16_t)((cVal & (0x7 << 11)) >> 11);
        uint16_t cAdd  = (uint16_t)((cVal & (0x7F << 4)) >> 4); //( cL1Word.to_ulong() & (0x7F << 4)) << 4;
        uint16_t cWdth = (uint16_t)((cVal & (0x7 << 1)) >> 1);  //( cL1Word.to_ulong() & (0x7 << 1)) << 1;
        uint16_t cMip  = (uint16_t)((cVal & (0x1 << 0)) >> 0);  //( cL1Word.to_ulong() & (0x1 << 0)) << 0;

        uint8_t cChipId = cId; //((*cIterator) & ((0x7) << (0 + 1 + 3 + 7))) >> (0 + 1 + 3 + 7);

        auto cChipIdMapped = this->getChipIdMapped(pHybridId, pReadoutChipId);
        // LOG(INFO) << BOLDBLUE << "Retreiving strip cluster information for Hybrid#" << +pHybridId << " Chip#" << +cChipId << " this is chip Id #" << +cChipIdMapped << " in CIC land" << RESET;
        if(cChipId == cChipIdMapped)
        {
            // LOG (INFO) << BOLDGREEN << "SCLUS ..... " << std::bitset<14>(*cIterator)  << RESET;
            SCluster cSCluster;
            cSCluster.fAddress = cAdd;  //((*cIterator) & ((0x7F) << (0 + 1 + 3))) >> (0 + 1 + 3);
            cSCluster.fWidth   = cWdth; //((*cIterator) & ((0x7) << (0 + 1))) >> (0 + 1);
            cSCluster.fMip     = cMip;  //((*cIterator) & ((0x1) << 0)) >> 0;
            cSClusters.push_back(cSCluster);
            // LOG(INFO) << BOLDYELLOW << "S-cluster in chip " << +pReadoutChipId << ", address : " << unsigned(cSCluster.fAddress) << "," << unsigned(cSCluster.fWidth) << ","
            //            << unsigned(cSCluster.fMip) << RESET;
        }
        cIterator++;
    };
    return cSClusters;
}

std::bitset<RAW_L1_CBC> DTCCicEvent::getRawL1Word(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    auto   cChipIdMapped = this->getChipIdMapped(pHybridId, pReadoutChipId);
    size_t cIndx         = cChipIdMapped;
    size_t cHybridIndex  = getHybridIndex(pHybridId);
    if(cHybridIndex >= fEventRawList.size())
    {
        throw Exception("DTCCicEvent::getRawL1Word - hybrid index out of range - " + std::to_string(cHybridIndex) + " out of: " + std::to_string(fEventRawList.size()));
    }
    if(cIndx >= fEventRawList.at(cHybridIndex).second.size())
    {
        throw Exception("DTCCicEvent::getRawL1Word - chip index out of range" + std::to_string(cIndx) + " out of: " + std::to_string(fEventRawList.at(cHybridIndex).second.size()));
    }
    auto& cDataBitset = fEventRawList[cHybridIndex].second[cIndx];
    return cDataBitset;
}

std::string DTCCicEvent::HexString() const
{
    LOG(INFO) << BOLDRED << "DTCCicEvent::HexString NOT IMPLEMENTED" << RESET;
    return "";
}

std::string DTCCicEvent::DataHexString(uint8_t pHybridId, uint8_t pCbcId) const
{
    pCbcId = pCbcId % 8;
    std::stringbuf tmp;
    std::ostream   os(&tmp);
    std::ios       oldState(nullptr);
    oldState.copyfmt(os);
    os << std::hex << std::setfill('0');

    // get the CBC event for pHybridId and pCbcId into vector<32bit> cbcData
    std::vector<uint32_t> cbcData;
    GetCbcEvent(pHybridId, pCbcId, cbcData);

    // l1cnt
    os << std::setw(3) << ((cbcData.at(2) & 0x01FF0000) >> 16) << std::endl;
    // pipeaddr
    os << std::setw(3) << ((cbcData.at(2) & 0x000001FF) >> 0) << std::endl;
    // trigdata
    os << std::endl;
    os << std::setw(8) << cbcData.at(3) << std::endl;
    os << std::setw(8) << cbcData.at(4) << std::endl;
    os << std::setw(8) << cbcData.at(5) << std::endl;
    os << std::setw(8) << cbcData.at(6) << std::endl;
    os << std::setw(8) << cbcData.at(7) << std::endl;
    os << std::setw(8) << cbcData.at(8) << std::endl;
    os << std::setw(8) << cbcData.at(9) << std::endl;
    os << std::setw(8) << ((cbcData.at(10) & 0xFFFFFFFC) >> 2) << std::endl;
    // stubdata
    os << std::setw(8) << cbcData.at(13) << std::endl;
    os << std::setw(8) << cbcData.at(14) << std::endl;

    os.copyfmt(oldState);

    return tmp.str();
}

bool     DTCCicEvent::Error(uint8_t pHybridId, uint8_t pCbcId, uint32_t i) const { return Bit(pHybridId, pCbcId, D19C_OFFSET_ERROR_CBC3); }

uint16_t DTCCicEvent::L1Status(uint8_t pHybridId) const
{
    // now only 1 bit per chip - OR of a few error flags
    if(fIsSparsified)
    {
        auto& cHitInformation = fEventHitList[getHybridIndex(pHybridId)].first;
        return cHitInformation.second;
    }
    else
        return 0xFFFF;
}

uint32_t DTCCicEvent::Error(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    // now only 1 bit per chip - OR of a few error flags
    if(fIsSparsified)
    {
        auto&   cHitInformation = fEventHitList[getHybridIndex(pHybridId)].first;
        uint8_t cChipIdMapped   = 0;
        if(pReadoutChipId < 8) cChipIdMapped = 1 + this->getChipIdMapped(pHybridId, pReadoutChipId);
        // auto     cChipIdMapped   = std::distance(fFeMapping.begin(), std::find(fFeMapping.begin(), fFeMapping.end(), pReadoutChipId));
        uint32_t cError = (cHitInformation.second & (0x1 << (cChipIdMapped))) >> (cChipIdMapped);

        return cError;
    }
    else
    {
        auto           cDataBitset = getRawL1Word(pHybridId, pReadoutChipId);
        std::bitset<2> cErrorBits(0);
        size_t         cOffset = 0;
        for(size_t cIndex = 0; cIndex < cErrorBits.size(); cIndex++) cErrorBits[cErrorBits.size() - 1 - cIndex] = cDataBitset[cDataBitset.size() - cOffset - 1 - cIndex];
        return (uint32_t)(cErrorBits.to_ulong());
    }
}

uint32_t DTCCicEvent::BxId(uint8_t pHybridId) const
{
    auto& cStubInformation = fEventStubList[getHybridIndex(pHybridId)].first;
    return cStubInformation.first;
}

uint16_t DTCCicEvent::Status(uint8_t pHybridId) const
{
    auto& cStubInformation = fEventStubList[getHybridIndex(pHybridId)].first;
    return cStubInformation.second;
}

uint32_t DTCCicEvent::L1Id(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    if(fIsSparsified)
    {
        auto& cHitInformation = fEventHitList[getHybridIndex(pHybridId)].first;
        return cHitInformation.first;
    }
    else
    {
        auto cDataBitset = getRawL1Word(pHybridId, pReadoutChipId);
        LOG(DEBUG) << BOLDBLUE << "Raw L1 Word is " << cDataBitset << RESET;
        std::bitset<9> cL1Id(0);
        size_t         cOffset = 9 + 2;
        for(size_t cIndex = 0; cIndex < cL1Id.size(); cIndex++) cL1Id[cL1Id.size() - 1 - cIndex] = cDataBitset[cDataBitset.size() - cOffset - 1 - cIndex];
        return cL1Id.to_ulong();
    }
}

// does not apply for sparsified event
uint32_t DTCCicEvent::PipelineAddress(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    if(fIsSparsified) { return 666; }
    else
    {
        auto           cDataBitset = getRawL1Word(pHybridId, pReadoutChipId);
        std::bitset<9> cPipeline(0);
        size_t         cOffset = 2;
        for(size_t cIndex = 0; cIndex < cPipeline.size(); cIndex++) cPipeline[cPipeline.size() - 1 - cIndex] = cDataBitset[cDataBitset.size() - cOffset - 1 - cIndex];
        return cPipeline.to_ulong();
    }
}

std::bitset<NMPACHANNELS> DTCCicEvent::decodePClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    LOG(INFO) << BOLDRED << "DTCCicEvent::decodePClusters - pixel related functions have not been tested yet" << RESET;
    auto&                     cClusterWords = fEventHitList[getHybridIndex(pHybridId)].second;
    std::bitset<NMPACHANNELS> cBitSet(0);
    size_t                    cClusterId = 0;
    for(auto cCluster: cClusterWords)
    {
        uint8_t cChipId       = (cCluster & ((0x7) << (0 + 4 + 3 + 7))) >> (0 + 4 + 3 + 7);
        auto    cChipIdMapped = this->getChipIdMapped(pHybridId, pReadoutChipId);
        if(cChipId == cChipIdMapped)
        {
            // figure out if it is an S or a P cluster
            uint8_t cFlag = (cCluster & (0x1 << 31)) >> 31;
            if(cFlag == 0) // s cluster
            {
                SCluster cSCluster;
                cSCluster.fAddress = (cCluster & ((0x7F) << (0 + 1 + 3))) >> (0 + 1 + 3);
                cSCluster.fWidth   = (cCluster & ((0x7) << (0 + 1))) >> (0 + 1);
                cSCluster.fMip     = (cCluster & ((0x1) << 0)) >> 0;
                // cSClusters.push_back(cSCluster);
                LOG(DEBUG) << BOLDRED << "S-cluster, address : " << unsigned(cSCluster.fAddress) << "," << unsigned(cSCluster.fWidth) << "," << unsigned(cSCluster.fMip) << RESET;
            }
            else
            {
                PCluster aPCluster;
                aPCluster.fAddress = (cCluster & ((0x7F) << (0 + 4 + 3))) >> (0 + 4 + 3);
                aPCluster.fWidth   = (cCluster & ((0x7) << (0 + 4))) >> (0 + 4);
                aPCluster.fZpos    = (cCluster & ((0xF) << 0)) >> 0;
                // cPClusters.push_back(aPCluster);
                LOG(DEBUG) << BOLDGREEN << "P-cluster, address : " << unsigned(aPCluster.fAddress) << "," << unsigned(aPCluster.fWidth) << "," << unsigned(aPCluster.fZpos) << RESET;
            }
            cClusterId++;
        }
    }
    return cBitSet;
}

std::bitset<NCHANNELS> DTCCicEvent::decodeClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    auto&                  cClusterWords = fEventHitList[getHybridIndex(pHybridId)].second;
    std::bitset<NCHANNELS> cBitSet(0);
    size_t                 cClusterId = 0;
    // LOG (INFO) << BOLDBLUE << "Decoding clusters for Hybrid" << +pHybridId << " readout chip " << +pReadoutChipId << RESET;
    if(fIs2S)
    {
        for(auto cClusterWord: cClusterWords)
        {
            uint8_t cChipId       = (cClusterWord & (0x7 << 11)) >> 11;
            auto    cChipIdMapped = this->getChipIdMapped(pHybridId, cChipId);
            if(cChipIdMapped != pReadoutChipId) continue;

            uint8_t cLayerId      = ((cClusterWord & (0xFF << 3)) >> 3) & 0x01;        // LSB is the layer
            uint8_t cStrip        = (((cClusterWord & (0xFF << 3)) >> 3) & 0xFE) >> 1; // strip id
            uint8_t cWidth        = 1 + (cClusterWord & 0x7);
            uint8_t cFirstChannel = 2 * cStrip + cLayerId;

            LOG(INFO) << BOLDBLUE << "Cluster " << +cClusterId << " : " << std::bitset<CLUSTER_WORD_SIZE>(cClusterWord) << "... " << +cWidth << " strip cluster in strip " << +cStrip << " in layer "
                      << +cLayerId << " so first hit is in channel " << +cFirstChannel << " of chip " << +cChipId << " [ real hybrid  " << +cChipIdMapped << " ]" << RESET;

            for(size_t cOffset = 0; cOffset < cWidth; cOffset++)
            {
                LOG(INFO) << BOLDBLUE << "\t\t\t\t.. hit in channel " << +(cFirstChannel + 2 * cOffset) << RESET;
                cBitSet[cFirstChannel + 2 * cOffset] = 1;
            }
            cClusterId++;
        }
    }
    // LOG(INFO) << BOLDBLUE << "Decoded clusters for Hybrid" << +pHybridId << " readout chip " << +pReadoutChipId << " : " << std::bitset<NCHANNELS>(cBitSet) << RESET;
    return cBitSet;
}

bool DTCCicEvent::DataBit(uint8_t pHybridId, uint8_t pReadoutChipId, uint32_t i) const
{
    if(fIsSparsified) { return (decodeClusters(pHybridId, pReadoutChipId)[i] > 0); }
    else
    {
        size_t cOffset = 2 + 9 + 9;
        return (getRawL1Word(pHybridId, pReadoutChipId)[cOffset + i] > 0);
    }
}

std::string DTCCicEvent::DataBitString(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    if(fIsSparsified)
    {
        std::string cBitStream = std::bitset<NCHANNELS>(this->decodeClusters(pHybridId, pReadoutChipId)).to_string();
        // LOG (DEBUG) << BOLDBLUE << "Original bit stream was : " << cBitStream << RESET;
        return cBitStream;
    }
    else
    {
        size_t      cOffset    = 2 + 9 + 9;
        std::string cBitStream = std::bitset<RAW_L1_CBC>(getRawL1Word(pHybridId, pReadoutChipId)).to_string();
        return cBitStream.substr(cOffset, RAW_L1_CBC);
    }
}

std::vector<bool> DTCCicEvent::DataBitVector(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    std::vector<bool> blist;
    if(fIsSparsified)
    {
        auto cDataBitset = this->decodeClusters(pHybridId, pReadoutChipId);
        for(uint8_t cPos = 0; cPos < NCHANNELS; cPos++) { blist.push_back(cDataBitset[cPos] == 1); }
    }
    else
    {
        size_t cOffset     = 2 + 9 + 9;
        auto   cDataBitset = getRawL1Word(pHybridId, pReadoutChipId);
        for(uint8_t cPos = 0; cPos < NCHANNELS; cPos++) { blist.push_back(cDataBitset[cDataBitset.size() - cOffset - 1 - cPos] == 1); }
    }
    return blist;
}

std::vector<bool> DTCCicEvent::DataBitVector(uint8_t pHybridId, uint8_t pReadoutChipId, const std::vector<uint8_t>& channelList) const
{
    std::vector<bool> blist;
    if(fIsSparsified)
    {
        auto cDataBitset = this->decodeClusters(pHybridId, pReadoutChipId);
        for(auto cChannel: channelList) { blist.push_back(cDataBitset[cChannel] == 1); }
    }
    else
    {
        size_t cOffset     = 2 + 9 + 9;
        auto   cDataBitset = getRawL1Word(pHybridId, pReadoutChipId);
        for(auto cChannel: channelList) { blist.push_back(cDataBitset[cDataBitset.size() - cOffset - 1 - cChannel] == 1); }
    }
    return blist;
}

std::string DTCCicEvent::GlibFlagString(uint8_t pHybridId, uint8_t pCbcId) const { return ""; }

std::vector<Stub> DTCCicEvent::StubVector(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    auto&             cStubWords = fEventStubList[getHybridIndex(pHybridId)].second;
    std::vector<Stub> cStubVec;
    for(auto cStubWord: cStubWords)
    {
        uint8_t cIdOffset      = (fIs2S) ? (8 + 4) : (8 + 4 + 3);
        uint8_t cAddressOffset = (fIs2S) ? (4) : (4 + 3);
        uint8_t cBendOffset    = (fIs2S) ? 0 : 4;
        uint8_t cBendMask      = (fIs2S) ? 0xF : 0x7;

        // 3 bit chip id
        auto    cChipIdMapped = this->getChipIdMapped(pHybridId, pReadoutChipId);
        uint8_t cChipId       = static_cast<uint8_t>((cStubWord & (0x7 << (cIdOffset))) >> cIdOffset);
        uint8_t cStubAddress  = static_cast<uint8_t>((cStubWord & (0xFF << (cAddressOffset))) >> cAddressOffset);
        uint8_t cStubBend     = static_cast<uint8_t>((cStubWord & (cBendMask << (cBendOffset))) >> cBendOffset);
        uint8_t cRow          = fIs2S ? 0x00 : static_cast<uint8_t>((cStubWord & 0xF));

        if(cChipId == cChipIdMapped)
        {
            LOG(DEBUG) << BOLDGREEN << "Stub package ..... " << std::bitset<18>(cStubWord) << " --  chip id from package " << +cChipId << " [ chip id on hybrid " << +pReadoutChipId << "]"
                       << " stub address is " << +cStubAddress << " stub bend is " << +cStubBend << " stub row is " << +cRow << RESET;
            cStubVec.emplace_back(cStubAddress, cStubBend, cRow);
        }
        // CI
    }
    return cStubVec;
}

std::string DTCCicEvent::StubBitString(uint8_t pHybridId, uint8_t pCbcId) const
{
    std::ostringstream os;

    std::vector<Stub> cStubVector = this->StubVector(pHybridId, pCbcId);

    for(auto cStub: cStubVector) os << std::bitset<8>(cStub.getPosition()) << " " << std::bitset<4>(cStub.getBend()) << " ";

    return os.str();
    // return BitString ( pHybridId, pCbcId, OFFSET_CBCSTUBDATA, WIDTH_CBCSTUBDATA );
}

bool DTCCicEvent::StubBit(uint8_t pHybridId, uint8_t pCbcId) const
{
    // here just OR the stub positions
    std::vector<Stub> cStubVector = this->StubVector(pHybridId, pCbcId);
    return (cStubVector.size() > 0);
}

uint32_t DTCCicEvent::GetNHits(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    uint32_t cNHits = 0;
    if(fIsSparsified)
    {
        // only 2S for now
        auto cDataBitset = this->decodeClusters(pHybridId, pReadoutChipId);
        cNHits           = cDataBitset.count();
    }
    else
    {
        size_t cOffset     = 2 + 9 + 9;
        auto   cDataBitset = this->getRawL1Word(pHybridId, pReadoutChipId);
        for(uint8_t cPos = 0; cPos < NCHANNELS; cPos++) { cNHits += (cDataBitset[cDataBitset.size() - cOffset - 1 - cPos] == 1); }
    }
    return cNHits;
}

std::vector<uint32_t> DTCCicEvent::GetHits(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    std::vector<uint32_t> cHits(0);
    if(fIsSparsified)
    {
        if(fIs2S)
        {
            for(auto cCluster: getClusters(pHybridId, pReadoutChipId))
            {
                uint8_t cFirstChannel = 2 * (cCluster.fFirstStrip - 127 * pReadoutChipId) + cCluster.fSensor;
                LOG(DEBUG) << BOLDMAGENTA << "Hybid#" << +pHybridId << " Chip#" << +pReadoutChipId << " Cluster in sensor " << +cCluster.fSensor << " in strip# " << +cCluster.fFirstStrip
                           << " actual strip " << +(cCluster.fFirstStrip - 127 * pReadoutChipId) << " of width " << +cCluster.fClusterWidth << RESET;
                for(int cId = 0; cId < cCluster.fClusterWidth; cId++)
                {
                    LOG(DEBUG) << BOLDMAGENTA << "\t\t.. hit in channel " << +cFirstChannel + 2 * cId << RESET;
                    cHits.push_back(cFirstChannel + cId * 2);
                }
            }
            // auto cDataBitset = this->decodeClusters(pHybridId, pReadoutChipId);
            // for(uint32_t i = 0; i < NCHANNELS; ++i)
            // {
            //     if(cDataBitset[i] > 0) { cHits.push_back(i); }
            // }
        }
        else
        {
            for(auto cCluster: GetPixelClusters(pHybridId, pReadoutChipId))
            {
                for(int cId = 0; cId <= cCluster.fWidth; cId++)
                {
                    uint32_t cHit = ((cCluster.fZpos + 1) << 24) | (cCluster.fAddress - 1) << 8 | cId << 0;
                    if(cCluster.fWidth > 0)
                        LOG(DEBUG) << BOLDBLUE << "Pixel cluster " << +cCluster.fZpos << " [z-pos]; " << +cCluster.fAddress << " [address] " << +cId << " [in cluster]"
                                   << " hit is " << +cHit << RESET;
                    cHits.push_back(cHit);
                }
            }
            for(auto cCluster: GetStripClusters(pHybridId, pReadoutChipId))
            {
                for(int cId = 0; cId <= cCluster.fWidth; cId++)
                {
                    uint32_t cHit = 0 << 24 | (cCluster.fAddress - 1) << 8 | cId << 0;
                    if(cCluster.fWidth > 0)
                        LOG(DEBUG) << BOLDGREEN << "Strip cluster " << +cCluster.fAddress << " [address] " << +cId << " [in cluster]"
                                   << " hit is " << +cHit << RESET;
                    cHits.push_back(cHit);
                }
            }
        }
    }
    else
    {
        if(fIs2S)
        {
            size_t cOffset     = 2 + 9 + 9;
            auto   cDataBitset = this->getRawL1Word(pHybridId, pReadoutChipId);
            //LOG(INFO) << "Hybrid: " << static_cast<int>(pHybridId) << " - readout chip: " << static_cast<int>(pReadoutChipId) << RESET;
            //LOG(INFO) << "cDataBitset " << cDataBitset << RESET;
            for(uint8_t cPos = 0; cPos < NCHANNELS; cPos++)
            {
                if(cDataBitset[cDataBitset.size() - cOffset - 1 - cPos] == 1)
                {
                    cHits.push_back(cPos);
                }
            }
            //LOG(INFO) << BOLDYELLOW << "nHits  " << cHits.size() << RESET;
            //getchar();
        }
        else
        {
            // To-DO add here for raw PS data
            LOG(INFO) << "DTCCicEvent::GetHits -- unsparsified -- PS" << RESET;
            throw Exception("DTCCicEvent::GetHits -- unsparsified -- PS -- not implemented yet");
        }
    }
    return cHits;
}

std::vector<uint64_t> DTCCicEvent::GetHitsLocalCoord(ReadoutChip* pChip)
{
    std::map<uint8_t, uint8_t> cOffsets;
    cOffsets[3] = 0;
    cOffsets[2] = 1;
    cOffsets[1] = 2;
    cOffsets[0] = 3;

    // for now only for CCB
    std::vector<uint64_t> cHitMap;
    if(pChip->getFrontEndType() != FrontEndType::CBC3) return cHitMap;
    auto     cHits      = GetHits(pChip->getHybridId(), pChip->getId());
    int      cFrstStrip = 0xFFFFFFF;
    int      cLstStrip  = 0;
    uint64_t localX     = pChip->getHybridId() % 2;
    uint16_t cOffset    = pChip->getId() * pChip->size() / 2;
    int      cNBins     = 8 * 127;
    for(auto cChnl = 0; cChnl < (int)pChip->size(); cChnl++)
    {
        uint64_t cHitFound    = (std::find(cHits.begin(), cHits.end(), cChnl) != cHits.end()) ? 1 : 0;
        uint16_t cStripOffset = cOffset;
        uint16_t cStripId     = cStripOffset + cChnl / 2.;
        if(pChip->getHybridId() % 2 == 0) // RHS hybrid first strip is in ROC#0
        {
            cStripId = cNBins - (cOffset + cChnl / 2);
        }
        // if( cHitFound ) LOG (INFO) << BOLDYELLOW << "Cic#" << +pChip->getHybridId() << " ROC#" << +pChip->getId()
        //     << " Chnl" << cChnl << " Sensor" << (cChnl%2)
        //     << " Strip#" << cStripId << RESET;
        uint64_t cSensorId = cOffsets[pChip->getOpticalGroupId()] * 2 + (cChnl % 2 != 0);
        uint64_t localY    = cStripId;
        uint64_t cId       = (cSensorId << 32) | (localX << 16) | localY;
        if(cHitFound == 1) cHitMap.push_back(cId);
        if((int)localY <= cFrstStrip) cFrstStrip = localY;
        if((int)localY >= cLstStrip) cLstStrip = localY;
    }
    // if( GetEventCount() == 1 && pChip->getOpticalGroupId() == 0 ) LOG (INFO) << BOLDYELLOW << "H#" << +pChip->getHybridId() << " ROC#" << +pChip->getId()
    //     << " first strip " << cFrstStrip
    //     << " last strip " << cLstStrip
    //     << " hit map has " << cHitMap.size() << " entries"
    //     << RESET;
    return cHitMap;
}

void DTCCicEvent::printL1Header(std::ostream& os, uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    os << GREEN << "HybridId = " << +pHybridId << " CBCId = " << +pReadoutChipId << RESET << std::endl;
    os << YELLOW << "L1 Id: " << this->L1Id(pHybridId, pReadoutChipId) << RESET << std::endl;
    os << YELLOW << "Bx Id: " << this->BxId(pHybridId) << RESET << std::endl;
    os << RED << "Error: " << static_cast<std::bitset<1>>(this->Error(pHybridId, pReadoutChipId)) << RESET << std::endl;
    os << CYAN << "Total number of hits: " << this->GetNHits(pHybridId, pReadoutChipId) << RESET << std::endl;
}

void DTCCicEvent::print(std::ostream& os) const
{
    os << BOLDGREEN << "EventType: DTC CIC2" << RESET << std::endl;
    os << BOLDBLUE << "L1A Counter: " << this->GetEventCount() << RESET << std::endl;
    os << "          Be Id: " << +this->getBeBoardId() << std::endl;
    os << "Event Data size: " << +this->GetEventDataSize() << std::endl;
    os << "Bunch Counter: " << this->GetBunch() << std::endl;
    os << BOLDRED << "    TDC Counter: " << +this->GetTDC() << RESET << std::endl;
    os << BOLDRED << "    TLU Trigger ID: " << +this->GetExternalTriggerId() << RESET << std::endl;

    const int FIRST_LINE_WIDTH = 22;
    const int LINE_WIDTH       = 32;
    const int LAST_LINE_WIDTH  = 8;
    // still need to work this out for sparsified data
    if(!fIsSparsified)
    {
        for(uint8_t cHybridId = 0; cHybridId < fEventRawList.size(); cHybridId++)
        {
            for(size_t cReadoutChipId = 0; cReadoutChipId < fEventRawList[cHybridId].second.size(); cReadoutChipId++)
            {
                // print out information
                printL1Header(os, cHybridId, cReadoutChipId);

                std::vector<uint32_t> cHits = this->GetHits(cHybridId, cReadoutChipId);
                if(cHits.size() == NCHANNELS)
                    os << BOLDRED << "All channels firing!" << RESET << std::endl;
                else
                {
                    int cCounter = 0;
                    for(auto& cHit: cHits)
                    {
                        os << std::setw(3) << cHit << " ";
                        cCounter++;
                        if(cCounter == 10)
                        {
                            os << std::endl;
                            cCounter = 0;
                        }
                    }
                    os << RESET << std::endl;
                }
                // channel data
                std::string data(this->DataBitString(cHybridId, cReadoutChipId));
                os << "Ch. Data:      ";
                for(int i = 0; i < FIRST_LINE_WIDTH; i += 2) os << data.substr(i, 2) << " ";

                os << std::endl;

                for(int i = 0; i < 7; ++i)
                {
                    for(int j = 0; j < LINE_WIDTH; j += 2) os << data.substr(FIRST_LINE_WIDTH + LINE_WIDTH * i + j, 2) << " ";

                    os << std::endl;
                }
                for(int i = 0; i < LAST_LINE_WIDTH; i += 2) os << data.substr(FIRST_LINE_WIDTH + LINE_WIDTH * 7 + i, 2) << " ";
                os << std::endl;
                // stubs
                uint8_t cCounter = 0;
                os << BOLDCYAN << "List of Stubs: " << RESET << std::endl;
                for(auto& cStub: this->StubVector(cHybridId, cReadoutChipId))
                {
                    os << CYAN << "Stub: " << +cCounter << " Position: " << +cStub.getPosition() << " Bend: " << +cStub.getBend() << " Strip: " << cStub.getCenter() << RESET << std::endl;
                    cCounter++;
                }
            }
        }
    }
    os << std::endl;
}

std::vector<Cluster> DTCCicEvent::clusterize(uint8_t pHybridId) const
{
    // even hits ---> bottom sensor : cSensorId ==0 [bottom], cSensorId == 1 [top]
    std::vector<Cluster>   cClusters(0);
    auto&                  cClusterWords = fEventHitList[getHybridIndex(pHybridId)].second;
    std::bitset<NCHANNELS> cBitSet(0);
    for(auto cClusterWord: cClusterWords)
    {
        uint8_t cChipId       = (cClusterWord & (0x7 << 11)) >> 11;
        auto    cChipIdMapped = this->getChipIdMapped(pHybridId, cChipId);
        // auto    cChipIdMapped = std::distance(fFeMapping.begin(), std::find(fFeMapping.begin(), fFeMapping.end(), cChipId));

        Cluster cCluster;
        uint8_t cFirst         = ((cClusterWord & (0xFF << 3)) >> 3) & 0x7F;
        uint8_t cSensorId      = (((cClusterWord & (0xFF << 3)) >> 3) & (0x1 << 8)) >> 8;
        cCluster.fFirstStrip   = cChipIdMapped * 127 + std::floor(cFirst / 2.); // I think the MSB is the layer ...
        cCluster.fClusterWidth = 1 + (cClusterWord & 0x7);
        cCluster.fSensor       = cSensorId;
        cClusters.push_back(cCluster);
    }
    return cClusters;
}

// TO-DO : replace all get clusters with clusterize
std::vector<Cluster> DTCCicEvent::getClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const
{
    std::vector<Cluster>   cClusters(0);
    auto&                  cClusterWords = fEventHitList[getHybridIndex(pHybridId)].second;
    std::bitset<NCHANNELS> cBitSet(0);
    size_t                 cClusterId = 0;
    for(auto cClusterWord: cClusterWords)
    {
        uint8_t cChipId       = (cClusterWord & (0x7 << 11)) >> 11;
        auto    cChipIdMapped = this->getChipIdMapped(pHybridId, cChipId);
        LOG(DEBUG) << BOLDYELLOW << "Cluster in Chip#" << +pReadoutChipId << " which is " << +cChipIdMapped << RESET;
        if(cChipIdMapped != pReadoutChipId) continue;

        uint8_t cLayerId      = ((cClusterWord & (0xFF << 3)) >> 3) & 0x01;        // LSB is the layer
        uint8_t cStrip        = (((cClusterWord & (0xFF << 3)) >> 3) & 0xFE) >> 1; // strip id
        uint8_t cWidth        = 1 + (cClusterWord & 0x7);
        uint8_t cFirstChannel = 2 * cStrip + cLayerId;

        LOG(DEBUG) << BOLDBLUE << "Cluster " << +cClusterId << " : " << std::bitset<CLUSTER_WORD_SIZE>(cClusterWord) << "... " << +cWidth << " strip cluster in strip " << +cStrip << " in layer "
                   << +cLayerId << " so first hit is in channel " << +cFirstChannel << " of chip " << +cChipId << " [ real hybrid  " << +cChipIdMapped << " ]" << RESET;

        Cluster cCluster;
        cCluster.fSensor       = cLayerId;
        cCluster.fFirstStrip   = pReadoutChipId * 127 + cStrip;
        cCluster.fClusterWidth = cWidth;
        cClusters.push_back(cCluster);
        cClusterId++;
    }
    return cClusters;
}

SLinkEvent DTCCicEvent::GetSLinkEvent(BeBoard* pBoard) const
{
    uint32_t   cEvtCount = this->GetEventCount();
    uint16_t   cBunch    = static_cast<uint16_t>(this->GetBunch());
    uint32_t   cBeStatus = this->fBeStatus;
    SLinkEvent cEvent(EventType::VR, pBoard->getConditionDataSet()->getDebugMode(), FrontEndType::CBC3, cEvtCount, cBunch, SOURCE_ID);

    // get link Ids
    std::vector<uint8_t> cLinkIds(0);
    std::set<uint8_t>    cEnabledHybrids;
    for(auto cOpticalGroup: *pBoard)
    {
        cEnabledHybrids.insert(cOpticalGroup->getId());
        cLinkIds.push_back(cOpticalGroup->getId());
    }

    // payload for the status bits
    GenericPayload cStatusPayload;
    LOG(DEBUG) << BOLDBLUE << "Generating S-link event " << RESET;
    // for the hit payload
    GenericPayload cPayload;
    uint16_t       cCbcCounter = 0;
    // now stub payload
    std::string cStubString = "";
    std::string cHitString  = "";
    //
    GenericPayload cStubPayload;
    for(auto cOpticalGroup: *pBoard)
    {
        uint8_t cLinkId = cOpticalGroup->getId();
        // int cHybridWord=0;
        // int cFirstBitFePayload = cPayload.get_current_write_position();
        uint16_t cCbcPresenceWord   = 0;
        uint8_t  cHybridStubCounter = 0;
        auto     cPositionStubs     = cStubString.length();
        auto     cPositionHits      = cHitString.length();
        for(auto cHybrid: *cOpticalGroup)
        {
            uint8_t cHybridId = cHybrid->getId();
            for(auto cChip: *cHybrid)
            {
                uint8_t cChipId     = cChip->getId();
                auto    cDataBitset = getRawL1Word(cHybridId, cChipId);

                /*auto cIndex = 7 - std::distance( fFeMapping.begin() , std::find( fFeMapping.begin(), fFeMapping.end()
                , cChipId ) ) ; if( cIndex >= (int)fEventDataList[cHybridId].second.size() ) continue; auto& cDataBitset =
                fEventDataList[cHybridId].second[ cIndex ];*/

                uint32_t cError       = this->Error(cHybridId, cChipId);
                uint32_t cPipeAddress = this->PipelineAddress(cHybridId, cChipId);
                uint32_t cL1ACounter  = this->L1Id(cHybridId, cChipId);
                uint32_t cStatusWord  = cError << 18 | cPipeAddress << 9 | cL1ACounter;

                auto cNHits = this->GetNHits(cHybridId, cChip->getId());
                // now get the CBC status summary
                if(pBoard->getConditionDataSet()->getDebugMode() == SLinkDebugMode::ERROR)
                    cStatusPayload.append((cError != 0) ? 1 : 0);
                else if(pBoard->getConditionDataSet()->getDebugMode() == SLinkDebugMode::FULL)
                    // assemble the error bits (63, 62, pipeline address and L1A counter) into a status word
                    cStatusPayload.append(cStatusWord, 20);

                // generate the payload
                // the first line sets the cbc presence bits
                cCbcPresenceWord |= 1 << (cChipId + 8 * (cHybridId % 2));

                // 254 channels + 2 padding bits at the end
                std::bitset<NCHANNELS> cBitsetHitData;
                size_t                 cOffset = 2 + 9 + 9;
                for(uint8_t cPos = 0; cPos < NCHANNELS; cPos++)
                {
                    cBitsetHitData[NCHANNELS - 1 - cPos] = cDataBitset[cDataBitset.size() - cOffset - 1 - cPos];
                    // cBitsetHitData[NCHANNELS-1-cPos] = cDataBitset[cDataBitset.size() - cOffset - 1 -cPos];
                }
                LOG(DEBUG) << BOLDBLUE << "Original biset is " << std::bitset<RAW_L1_CBC>(cDataBitset) << RESET;
                // convert bitset to string.. this is useful in case I need to reverse this later
                std::string cOut = cBitsetHitData.to_string() + "00";
                LOG(DEBUG) << BOLDBLUE << "Packed biset is " << cOut << RESET;
                // std::reverse( cOut.begin(), cOut.end() );
                cHitString += cOut;
                if(cNHits > 0)
                {
                    LOG(DEBUG) << BOLDBLUE << "Readout chip " << +cChip->getId() << " on link " << +cLinkId << RESET;
                    //" : " << cBitsetHitData.to_string() << RESET;
                    auto cHits = this->GetHits(cHybridId, cChip->getId());
                    for(auto cHit: this->GetHits(cHybridId, cChip->getId())) { LOG(DEBUG) << BOLDBLUE << "\t... Hit in channel " << +cHit << RESET; }
                }
                // now stubs
                for(auto cStub: this->StubVector(cHybridId, cChip->getId()))
                {
                    std::bitset<16> cBitsetStubs(((cChip->getId() + 8 * (cHybridId % 2)) << 12) | (cStub.getPosition() << 4) | cStub.getBend());
                    cStubString += cBitsetStubs.to_string();
                    LOG(DEBUG) << BOLDBLUE << "\t.. stub in seed " << +cStub.getPosition() << " and bench code " << std::bitset<4>(cStub.getBend()) << RESET;
                    cHybridStubCounter += 1;
                }
                cCbcCounter++;
            } // end of CBC loop
        }     // end of Hybrid loop

        // for the hit payload, I need to insert the word with the number of CBCs at the index I remembered before
        // cPayload.insert (cCbcPresenceWord, cFirstBitFePayload );
        std::bitset<16> cHybridHeader(cCbcPresenceWord);
        cHitString.insert(cPositionHits, cHybridHeader.to_string());
        // for the stub payload .. do the same
        std::bitset<6> cStubHeader(((cHybridStubCounter << 1) | 0x00) & 0x3F); // 0 for 2S , 1 for PS
        LOG(DEBUG) << BOLDBLUE << "Hybrid " << +cLinkId << " hit header " << cHybridHeader << " - stub header  " << std::bitset<6>(cStubHeader) << " : " << +cHybridStubCounter
                   << " stub(s) found on this link." << RESET;
        cStubString.insert(cPositionStubs, cStubHeader.to_string());
    }
    for(size_t cIndex = 0; cIndex < 1 + cHitString.length() / 64; cIndex++)
    {
        auto            cTmp = cHitString.substr(cIndex * 64, 64);
        std::bitset<64> cBitset(cHitString.substr(cIndex * 64, 64));
        uint64_t        cWord = cBitset.to_ulong() << (64 - cTmp.length());
        LOG(DEBUG) << BOLDBLUE << "Hit word " << cTmp.c_str() << " -- word " << std::bitset<64>(cWord) << RESET;
        cPayload.append(cWord);
    }
    for(size_t cIndex = 0; cIndex < 1 + cStubString.length() / 64; cIndex++)
    {
        auto            cTmp = cStubString.substr(cIndex * 64, 64);
        std::bitset<64> cBitset(cStubString.substr(cIndex * 64, 64));
        uint64_t        cWord = cBitset.to_ulong() << (64 - cTmp.length());
        LOG(DEBUG) << BOLDBLUE << "Stub word " << cTmp.c_str() << " -- word " << std::bitset<64>(cWord) << RESET;
        cStubPayload.append(cWord);
    }
    std::vector<uint64_t> cStubData = cStubPayload.Data<uint64_t>();
    for(auto cStub: cStubData) { LOG(DEBUG) << BOLDBLUE << std::bitset<64>(cStub) << RESET; }
    LOG(DEBUG) << BOLDBLUE << +cCbcCounter << " CBCs present in this event... " << +cEnabledHybrids.size() << " Hybrids enabled." << RESET;

    cEvent.generateTkHeader(cBeStatus, cCbcCounter, cEnabledHybrids, pBoard->getConditionDataSet()->getCondDataEnabled(),
                            false); // Be Status, total number CBC, condition data?, fake data?
    // generate a vector of uint64_t with the chip status
    // if (pBoard->getConditionDataSet()->getDebugMode() != SLinkDebugMode::SUMMARY) // do nothing
    cEvent.generateStatus(cStatusPayload.Data<uint64_t>());

    // PAYLOAD
    cEvent.generatePayload(cPayload.Data<uint64_t>());

    // STUBS
    cEvent.generateStubs(cStubPayload.Data<uint64_t>());

    // condition data, first update the values in the vector for I2C values
    uint32_t cTDC = this->GetTDC();
    pBoard->updateCondData(cTDC);
    cEvent.generateConditionData(pBoard->getConditionDataSet());
    cEvent.generateDAQTrailer();
    return cEvent;
}
} // namespace Ph2_HwInterface
