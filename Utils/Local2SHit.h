#pragma once

class Local2SHit
{
  public:
    // counters from FW
    uint32_t fBeTriggerId;
    uint32_t fBeBunchCntr;
    // counter from CICs
    uint32_t fL1Id;
    uint32_t fBxId;
    // sensor Ids
    std::vector<uint8_t> fSensorId;
    // local coordinates in strips
    std::vector<uint16_t> fLocalX;
    std::vector<uint16_t> fLocalY;
    // valid for unsparsified
    std::vector<uint16_t> fLocalL1;
    std::vector<uint16_t> fLocalPipelineAddress;
};