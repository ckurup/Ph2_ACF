
#ifndef SFPMonitoringTool_h__
#define SFPMonitoringTool_h__

#include <map>
#include <string>
#include <vector>
namespace Ph2_HwInterface
{
class D19cSFPMonitoringInterface;
}
class SFPMonitoringTool
{
  public:
    SFPMonitoringTool(std::string puHalConfigFileName = "", uint16_t pBoardId = 0);
    ~SFPMonitoringTool();

    std::pair<float, float> GetMonitorable(std::string pParameter, std::string pMezzanine, uint8_t pLinkId);
    void                    setNMeasurements(size_t pN) { fNMeasurements = pN; }

  private:
    Ph2_HwInterface::D19cSFPMonitoringInterface* fMyInterface;
    std::string                                  fConfigurationFile{""};
    uint16_t                                     fBoardId{0};
    Ph2_HwInterface::D19cSFPMonitoringInterface  getSFPMonitoringInterface(const std::string& configurationFile, uint16_t boardId);
    size_t                                       fNMeasurements{10};
};

#endif