#ifndef _DTCFastCommandInterface_H__
#define _DTCFastCommandInterface_H__

#if defined(__EMP__)

#include "FastCommandInterface.h"
#include <vector>

namespace Ph2_HwInterface
{
class DTCFastCommandInterface : public FastCommandInterface
{
  public: // constructors
    DTCFastCommandInterface(const std::string& puHalConfigFileName, const std::string& pBoardId);
    DTCFastCommandInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCFastCommandInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCFastCommandInterface();

  public:
    void InitialiseTCDS(const Ph2_HwDescription::BeBoard* pBoard);

    void SendGlobalReSync(uint8_t pDuration = 0) override;               // 1 clk cycle  ;
    void SendGlobalCalPulse(uint8_t pDuration = 0) override;             // 1 clk cycle  ;
    void SendGlobalL1A(uint8_t pDuration = 0) override;                  // 1 clk cycle  ;
    void SendGlobalCounterReset(uint8_t pDuration = 0) override;         // 1 clk cycle  ;
    void SendGlobalCounterResetResync(uint8_t pDuration = 0) override;   // 1 clk cycle  ;
    void SendGlobalCounterResetL1A(uint8_t pDuration = 0) override;      // 1 clk cycle  ;
    void SendGlobalCounterResetCalPulse(uint8_t pDuration = 0) override; // 1 clk cycle  ;
    void SendGlobalCustomFastCommands(std::vector<FastCommand>& pFastCmd) override;
    void ComposeFastCommand(const FastCommand& pFastCommand) override {}
    void SendGlobalRepetitiveL1A(unsigned pNtriggers);

    // local fast commands
    void SendReSync(unsigned pChanId);
    void SendCalPulse(unsigned pChanId);
    void SendL1A(unsigned pChanId);
    void SendCounterReset(unsigned pChanId);
    void SendCounterResetL1A(unsigned pChanId);
    void SendCounterResetCalPulse(unsigned pChanId);
    void SendRepetitiveL1As(unsigned pChanId, unsigned pNtriggers);

    // channel delays
    void SetFastCommandDelays(unsigned pChanId, unsigned pDelay);

    // custom fast commands
    void SendFastCommands(std::vector<FastCommand>& pFastCmd, signed pRepeat);
    void SendFastCommands(unsigned pChanId, std::vector<FastCommand>& pFastCmd, signed pRepeat);

    // helper functions
    void PrintLocalCounters(unsigned pChanId);
    void PrintGlobalCounters();

  private:
    void InitialiseLocalTCDS(unsigned pChanId);
    void GeneratePattern(std::vector<FastCommand>& pFastCmd, std::vector<unsigned>& pPatterns, bool repeat);

    void LocalStartPatternGen(unsigned pChanId);
    void LocalResetPatternGen(unsigned pChanId);
    void LocalLoadPatternGen(unsigned pChanId, std::vector<unsigned>& pPatterns);
    void LocalSourceSelect(unsigned pChanId, unsigned pSource);
    void LocalRepeatFastCommand(unsigned pChanId, unsigned pRepeat);

    void GlobalStartPatternGen();
    void GlobalResetPatternGen();
    void GlobalLoadPatternGen(std::vector<unsigned>& pPatterns);
    void GlobalSourceSelect(unsigned pSource);
    void GlobalRepeatFastCommand(unsigned pRepeat);


  private:
    std::vector<uint8_t> fChanIds;

};
} // namespace Ph2_HwInterface
#endif
#endif
