#ifndef _DTCLinkInterface_H__
#define _DTCLinkInterface_H__

#if defined(__EMP__)

#include "LinkInterface.h"
#include "emp/Controller.hpp"

#define LPGBTFPGA 2.1
namespace Ph2_HwInterface
{
class DTCLinkInterface : public LinkInterface
{
  public:
    DTCLinkInterface(const std::string& puHalConfigFileName, const std::string& pBoardId);
    DTCLinkInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCLinkInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCLinkInterface();

  public:
    uint32_t fWait_ms = 100;
    void     ResetLinks() override;
    bool     GetLinkStatus(uint8_t pLinkId = 0) override;
    void     GeneralLinkReset(const Ph2_HwDescription::BeBoard* pBoard) override;
    // function to link FEConfigurationInterface
    void LinkEMPcontroller(emp::Controller* pController) { fController = pController; }

  private:
    emp::Controller* fController{nullptr};
    void             SetQuad(uint32_t q);
    void             SetChan(uint32_t c);
    //--------------------------------
    // Link reset
    //--------------------------------
    bool resetGbtLink(uint8_t pLinkId);
    void resetLpGbtTx(uint8_t pQuadId);
    void resetLpGbtRx(uint8_t pQuadId);
    bool LpGbtDownlinkRdy(uint8_t pLinkId);
    bool LpGbtUplinkRdy(uint8_t pLinkId);
    //
    void resetSCC(unsigned pLinkId);
};
} // namespace Ph2_HwInterface
#endif
#endif