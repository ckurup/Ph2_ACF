#include "HWInterface/DTCL1ReadoutInterface.h"
#include "Utils/FileHandler.h"
using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
DTCL1ReadoutInterface::DTCL1ReadoutInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : L1ReadoutInterface(pId, pUri, pAddressTable) {}
DTCL1ReadoutInterface::DTCL1ReadoutInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : L1ReadoutInterface(puHalConfigFileName, pBoardId) {}
DTCL1ReadoutInterface::DTCL1ReadoutInterface(const std::string& puHalConfigFileName, const std::string& pBoardId) : L1ReadoutInterface(puHalConfigFileName, pBoardId) {}
DTCL1ReadoutInterface::~DTCL1ReadoutInterface() {}

bool DTCL1ReadoutInterface::SetupForReadout()
{
    LOG(INFO) << BOLDRED << "DTCL1ReadoutInterface::SetupForReadout NOT IMPLEMENTED" << RESET;
    return false;
}

bool DTCL1ReadoutInterface::WaitForNTriggers()
{
    LOG(INFO) << BOLDBLUE << "DTCL1ReadoutInterface::WaitForNTriggers NOT yet IMPLEMENTED" << RESET;
    return false;
}

bool DTCL1ReadoutInterface::WaitForData() { return true; }

bool DTCL1ReadoutInterface::WaitForReadout() 
{
    LOG(INFO) << BOLDBLUE << "DTCL1ReadoutInterface::WaitForReadout NOT IMPLEMENTED" << RESET;
    return true;
}

void DTCL1ReadoutInterface::StartReadout()
{
    LOG(INFO) << BOLDBLUE << "DTCL1ReadoutInterface::StartReadout NOT IMPLEMENTED" << RESET;
}
void DTCL1ReadoutInterface::StopReadout()
{
    LOG(INFO) << BOLDBLUE << "DTCL1ReadoutInterface::StopReadout NOT IMPLEMENTED" << RESET;
}

bool DTCL1ReadoutInterface::ReadEvents(const Ph2_HwDescription::BeBoard* pBoard) {
    if(fFastCommandInterface == nullptr) 
    {
        LOG(ERROR) << BOLDRED << "ERROR! Fast command interface is not initialized!" << RESET;
        throw Exception("Fast command interface is not initialized");
    }
    int nAttempts = 0;
    while(nAttempts < 5){
        int nTriggersToSend = 0;
        int nTriggersSent = 0;
        fReceivedEvents = 0;
        fData.clear();
        while (nTriggersSent < static_cast<int>(fNEvents)) 
        {
            nTriggersToSend = std::min(1, (static_cast<int>(fNEvents) - nTriggersSent));
            //LOG(INFO) << BOLDBLUE << "Sending " << nTriggersToSend << " triggers" << RESET;
            LOG(INFO) << BOLDBLUE << "####################################################################################################"<< RESET;
            LOG(INFO) << BOLDBLUE << "Sending Trigger no. " << nTriggersSent + 1<< RESET;
            //LOG(INFO) << BOLDBLUE << "TriggersSent " << nTriggersSent << RESET;
            static_cast<DTCFastCommandInterface*>(fFastCommandInterface)->SendGlobalRepetitiveL1A(nTriggersToSend);
            nTriggersSent += nTriggersToSend;
            LOG(INFO) << BOLDBLUE << "Start of filling data for this trigger"<< RESET;
            this->FillData();
            LOG(INFO) << BOLDBLUE << "End filling data for this trigger"<< RESET;
        }
        if(fReceivedEvents == static_cast<int>(fNEvents) || fReceivedEvents*2 == static_cast<int>(fNEvents)) {
            LOG(INFO) << "Successfully received " << fReceivedEvents << " events";
            return true;
        }
        else {
            LOG(ERROR) << BOLDRED << "ERROR! Number of received events = " << fReceivedEvents << " is not equal to the number of requested events = " << fNEvents << RESET;
            nAttempts++;
            LOG(ERROR) << BOLDRED << "Retrying - attempt number: " << nAttempts << RESET;
        }
    }
    throw Exception("ERROR: Number of received events is not equal to the number of requested events, attempted 5 times - giving up");
}

bool DTCL1ReadoutInterface::PollReadoutData(const Ph2_HwDescription::BeBoard* pBoard, bool pWait) {
    LOG(DEBUG) << BOLDYELLOW << "DTCL1ReadoutInterface::PollReadoutData" << RESET;
    fData.clear();
    this->FillData();
    return true;
}

bool DTCL1ReadoutInterface::ResetReadout() {
    //LOG(DEBUG) << "DTCL1ReadoutInterface::ResetReadout - manually cleaning out output FIFOs";
    LOG(INFO)<<BOLDYELLOW << "DTCL1ReadoutInterface::ResetReadout - manually cleaning out output FIFOs"<< RESET;
    //unsigned int cOutputFIFO = 0;
    while(this->CheckStatusDaqpathOutput()) 
    {
        auto cReadIDNW = ReadDaqpathIdNw();
        unsigned int cNWords  = static_cast<unsigned int>(cReadIDNW & 0x0000FFFF);//number of words in this event
        for(unsigned int i = 0; i < cNWords; i++) 
        {
            ReadDaqpathDwh();
            ReadDaqpathDwl();
        }
    }//while loop
    return !this->CheckStatusDaqpathOutput();
}

bool DTCL1ReadoutInterface::DumpFifo() 
{
    //auto cOutputFIFO = 0;
    LOG(INFO)<<BOLDYELLOW << "DTCL1ReadoutInterface::DumpFifo - Inside the function"<< RESET;
    while(this->CheckStatusDaqpathOutput())
    {
        auto cReadIDNW = this->ReadDaqpathIdNw();
        unsigned int cNWords  = static_cast<unsigned int>(cReadIDNW & 0x0000FFFF);//number of words in this event
        LOG(INFO) << "Event ID: " << std::hex << (cReadIDNW >> 16) << std::dec << " - n 32b words: " << cNWords*2 << RESET;
        for(unsigned int i = 0; i < cNWords; i++) 
        {
            LOG(INFO) << std::hex << static_cast<unsigned>(ReadDaqpathDwh()) << " " << static_cast<unsigned>(ReadDaqpathDwl()) << RESET;
        }
    }
    return !this->CheckStatusDaqpathOutput();
}

bool DTCL1ReadoutInterface::CheckBuffers() {
    LOG(INFO) << BOLDBLUE << "DTCL1ReadoutInterface::CheckBuffers NOT IMPLEMENTED" << RESET;
    return false;
}

void DTCL1ReadoutInterface::FillData()
{
    //TODO: Works only with daqpath header enabled
    while(this->CheckStatusDaqpathOutput())
    {
        LOG(INFO) << BOLDGREEN << "Reading daqpath_out.LINK0.IDNW_FIFO -> "<< std::bitset<32>(ReadDaqpathIdNw()) << RESET;
        uint32_t cReadIDNW = ReadDaqpathIdNw();
        fData.push_back(cReadIDNW);
        uint32_t cEventId = static_cast<uint32_t> (cReadIDNW >> 16);//eventID
        uint32_t cNWords  = static_cast<uint32_t> (cReadIDNW & 0xFFFF);//number of words in this event
	      LOG(INFO) << GREEN << "Event Id = "<< cEventId << RESET;
        //LOG(INFO) << BOLDGREEN << "Event id: " << cEventId << " - n 32b words: " << std::dec << cNWords*2 << RESET; //Event id should match the one from the be - otherwise you get just two error words
        if(cNWords<=2) 
        {
            //LOG(ERROR) << BOLDRED << "WARNING: Error event from the daqpath - only 2 words - skipping" <<RESET;
            LOG(INFO) << BOLDBLUE << "WARNING: Event from the daqpath has only 2 words" <<RESET;
            //continue;
        }
        //read payload data for the current event
        for(unsigned int i = 0; i < cNWords; i++) {
            uint32_t cReadDwh = ReadDaqpathDwh();
            uint32_t cReadDwl = ReadDaqpathDwl(); 
            fData.push_back(cReadDwh);
            fData.push_back(cReadDwl);
            LOG(INFO) << GREEN <<"Reading daqpath_out.LINK0.DWH_FIFO and DWL_FIFO-> "<< std::bitset<32>(cReadDwh)<<" "<< std::bitset<32>(cReadDwl)<< RESET;
        }
        //if(cNWords>2) fReceivedEvents++;
        fReceivedEvents++;
    } //end of while loop
    //if(fReceivedEvents%10 == 0) LOG(INFO) << BOLDBLUE << "Number of received events = " << fReceivedEvents << RESET;
    /* if(fData.size() < 70) {
        throw Exception("DTCL1ReadoutInterface::FillData - Data size is " + std::to_string(fData.size()) +  " less than 70 words - IMPOSSIBLE FOR UNSPARSIFIED DATA");
    }
    */
}

uint32_t DTCL1ReadoutInterface::ReadDaqpathIdNw(unsigned int pOutId) 
{
    uint32_t readIDNW = fFastCommandInterface->ReadReg("payload.be_daq.daqpath_out.LINK"+std::to_string(pOutId)+".IDNW_FIFO");
    return readIDNW;
}

uint32_t DTCL1ReadoutInterface::ReadDaqpathDwh(unsigned int pOutId) 
{
    return fFastCommandInterface->ReadReg("payload.be_daq.daqpath_out.LINK"+std::to_string(pOutId)+".DWH_FIFO"); //first 32 bits of the event
}

uint32_t DTCL1ReadoutInterface::ReadDaqpathDwl(unsigned int  pOutId) 
{
    return fFastCommandInterface->ReadReg("payload.be_daq.daqpath_out.LINK"+std::to_string(pOutId)+".DWL_FIFO"); //second 32 bits of the event
}

bool DTCL1ReadoutInterface::CheckStatusDaqpathOutput(unsigned int pOutId) 
{
    if(pOutId >= 4) {
        LOG(ERROR) << BOLDRED << "ERROR! Uncorrect output fifo (OUT" << pOutId << ")" << RESET;
        throw Exception("DTCL1ReadoutInterface::CheckStatusDaqpathOutput - Uncorrect output fifo for daqpath - OUT" + std::to_string(pOutId) + " only 0 to 3 are allowed");
    }
    uint32_t Out_fifo_emptyReg = fFastCommandInterface->ReadReg("payload.be_daq.daqpath_ext_ctrl.S0");
    //uint32_t in_fifo_s1 = fFastCommandInterface->ReadReg("payload.be_daq.daqpath_ext_ctrl.S1");
    //uint32_t in_fifo_s2 = fFastCommandInterface->ReadReg("payload.be_daq.daqpath_ext_ctrl.S2");
    //uint32_t in_fifo_s3 = fFastCommandInterface->ReadReg("payload.be_daq.daqpath_ext_ctrl.S3");
    //LOG(INFO) << BOLDBLUE << "[0]daq FIFOs       S0 -> "<< std::showbase << std::hex << Out_fifo_emptyReg<< " S1 -> "<<in_fifo_s1<<" S2 -> "<<in_fifo_s2<<" S3 -> "<<in_fifo_s3<< std::dec << RESET;
    //if S0 == 0x00000003 then OUT0 is empty. if S0 == 0x00000030 then OUT1 is empty... and so on
    uint32_t cIsEmpty = 0x00000003 << 4*pOutId;
    if(Out_fifo_emptyReg & cIsEmpty) {
        return 0;
    }
    return 1;
}

void DTCL1ReadoutInterface::EnableDaqpathOutputReadout() 
{
    //bool status = WriteReg("payload.be_daq.daqpath_ext_ctrl.C0", 0x1);
    WriteReg("payload.be_daq.daqpath_ext_ctrl.C0", 0x1);
    LOG(INFO) << "DaqPath readout enabled " << RESET;
    //if(! WriteReg("payload.be_daq.daqpath_ext_ctrl.C0", 0x1)) {
    //    LOG(ERROR) << BOLDRED << "ERROR! Cannot write on payload.be_daq.daqpath_ext_ctrl.C0" << RESET;
    //    throw Exception("Cannot enable readout from output fifo in daqpath - payload.be_daq.daqpath_ext_ctrl.C0");
    //} 
}

void DTCL1ReadoutInterface::FeChannelFifoStatus(int pChan) 
{

    std::vector<std::pair<std::string, uint32_t>> regWriteVec{
        std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChan }
    };
    WriteStackReg(regWriteVec);

    auto status0{ fFastCommandInterface->ReadReg("payload.fe_chan.l1_daq.status0.fifo_empty") };
    auto status1{ fFastCommandInterface->ReadReg("payload.fe_chan.l1_daq.status1.fifo_empty") };

    LOG(INFO) << "Channel: " << pChan << " status0: " << std::hex << status0 << " status1: " << status1 << std::dec << " ------- 0: full | 7: empty " << RESET;
}

void DTCL1ReadoutInterface::SetDaqpathChannelMask(uint32_t pChMask) 
{
    fFastCommandInterface->WriteReg("payload.be_daq.daqpath_csr.C1", pChMask);
    LOG(INFO) << BOLDGREEN << "Setting channel mask to: 0x" << std::hex << std::setw(8) << std::setfill('0') << pChMask << std::dec << " 1 skipped channel - 0 kept, 1 channel for each CIC" << RESET;
}

void DTCL1ReadoutInterface::EnableDaqpathEventHeader(int pHEnable) 
{
    LOG(INFO) << BOLDGREEN << "Setting event header to: " << pHEnable << RESET;
    fFastCommandInterface->WriteReg("payload.be_daq.daqpath_csr.C0", pHEnable);
    if(pHEnable) {
        LOG(INFO) << BOLDGREEN << "Daqpath event header enabled" << RESET;
    }
    else{
        LOG(INFO) << BOLDGREEN << "Daqpath event header disabled" << RESET;
    }
}

bool DTCL1ReadoutInterface::IsDaqpathEventHeaderEnabled() 
{
    auto daqPathStatus = ReadReg("payload.be_daq.daqpath_csr.C0");
    LOG(DEBUG) << BOLDYELLOW << "DaqPath header is: " << daqPathStatus << " (0: disbaled \t 1: enabled)" << RESET;
    return daqPathStatus;
}

void DTCL1ReadoutInterface::ResetDaqPath()
{
    WriteReg("payload.be_daq.daqpath_ext_ctrl", 0x00000010);//reset the global system
    WriteReg("payload.be_daq.daqpath_ext_ctrl", 0x00000000);//toggling reset...
    this->EnableDaqPath();
}

void DTCL1ReadoutInterface::EnableDaqPath() 
{
    WriteReg("payload.be_daq.daqpath_ext_ctrl", 1);
    LOG(INFO) << BOLDBLUE << "DAQPATH enabled" << RESET;//if writing is done correctly WriteReg returns false. So I have an error when status == true
}



} // namespace Ph2_HwInterface
