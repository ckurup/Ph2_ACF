#ifndef _DTCTriggerInterface_H__
#define _DTCTriggerInterface_H__

#if defined(__EMP__)

#include "TriggerInterface.h"
#include "DTCFastCommandInterface.h"

namespace Ph2_HwInterface
{
class DTCTriggerInterface : public TriggerInterface
{
  public: // constructors
    DTCTriggerInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCTriggerInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCTriggerInterface();

  public: // virtual functions
    bool SetNTriggersToAccept(uint32_t pNTriggersToAccept) override 
    {
      fTriggerConfiguration.fNtriggersToAccept = pNTriggersToAccept;
      return true;
    }

    void ResetTriggerFSM() override 
    {
      LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::ResetTriggerFSM NOT IMPLEMENTED" << RESET;
    }
    
    void ReconfigureTriggerFSM(std::vector<std::pair<std::string, uint32_t>> pTriggerConfig) override 
    {
      LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::ReconfigureTriggerFSM NOT IMPLEMENTED" << RESET;
    }
    // void     Start() override { return true; }
    // void     Pause() override {}
    // void     Resume() override {}
    // bool     Stop() override { return true; }
    bool     RunTriggerFSM() override
    { 
      LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::RunTriggerFSM NOT IMPLEMENTED" << RESET;  
      return true;
    }
    
    uint32_t GetTriggerState() override { 
      LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::GetTriggerState NOT IMPLEMENTED" << RESET;
      return 0;
    }
    
    bool     WaitForNTriggers(uint32_t pNTriggers) override
    {
      LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::WaitForNTriggers NOT IMPLEMENTED" << RESET;
      return true;
    }
    bool     SendNTriggers(uint32_t pNTriggers) override;
    void     PrintStatus() override 
    {
      LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::PrintStatus NOT IMPLEMENTED" << RESET;
    }

    void     SetFastCommandInterface(DTCFastCommandInterface* pFastCommandInterface) { fFastCommandInterface = pFastCommandInterface; }
    void     SetChannelToSendTriggersTo(uint32_t pChannel) { fChannelToSendTriggersTo = pChannel; }

  private:
    DTCFastCommandInterface* fFastCommandInterface;
    uint32_t fChannelToSendTriggersTo = 999;
   
};
} // namespace Ph2_HwInterface
#endif
#endif