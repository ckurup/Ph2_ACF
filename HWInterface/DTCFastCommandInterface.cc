#if defined(__EMP__)

#include "DTCFastCommandInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{

DTCFastCommandInterface::DTCFastCommandInterface(const std::string& puHalConfigFileName, const std::string& pBoardId) : FastCommandInterface(puHalConfigFileName, pBoardId)
{
    LOG(INFO) << BOLDYELLOW << "DTCFastCommandInterface::DTCFastCommandInterface Constructor" << RESET; 
    LOG(INFO) << BOLDYELLOW << puHalConfigFileName << "\t" << pBoardId << RESET;
    fChanIds.clear();
}

DTCFastCommandInterface::DTCFastCommandInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : FastCommandInterface(pId, pUri, pAddressTable)
{
    fChanIds.clear();
}

DTCFastCommandInterface::DTCFastCommandInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : FastCommandInterface(puHalConfigFileName, pBoardId) { fChanIds.clear(); }

DTCFastCommandInterface::~DTCFastCommandInterface() {}

void DTCFastCommandInterface::InitialiseTCDS(const BeBoard* pBoard)
{
    LOG(INFO) << BOLDYELLOW << "Initializing global TCDS" << RESET;
    GlobalResetPatternGen();
    GlobalSourceSelect(15);      // mask (resync, l1a, cal pulse, bc0)
    GlobalRepeatFastCommand(0); // repeat 1->1023 times, 0=infinite repeat (if set by pattern gen)

    fChanIds.clear();

    for(int i=0;(unsigned int)i<ReadReg("payload.fe.num_links"); i++){
        fChanIds.push_back(i);
        InitialiseLocalTCDS(i);
    }
}

void DTCFastCommandInterface::InitialiseLocalTCDS(unsigned pChanId)
{
    LOG(INFO) << BOLDYELLOW << "Initializing local TCDS on Channel#" << pChanId << RESET;
    //WriteReg("payload.fe.chan_sel", pChanId);
    SetFastCommandDelays(pChanId, 0);  // 0->7 bx delay on output

    LocalResetPatternGen(pChanId);
    LocalSourceSelect(pChanId, 15);     // mask (resync, l1a, cal pulse, bc0)
    LocalRepeatFastCommand(pChanId, 0); // repeat 1->1023 times, 0=infinite repeat (if set by pattern gen)
}

void DTCFastCommandInterface::SendGlobalReSync(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendReSync(nullChanId);
}

void DTCFastCommandInterface::SendGlobalCalPulse(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendCalPulse(nullChanId);
}

void DTCFastCommandInterface::SendGlobalL1A(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendL1A(nullChanId);
}

void DTCFastCommandInterface::SendGlobalCounterReset(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendCounterReset(nullChanId);
}

void DTCFastCommandInterface::SendGlobalCounterResetResync(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendReSync(nullChanId);
}

void DTCFastCommandInterface::SendGlobalCounterResetL1A(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendCounterResetL1A(nullChanId);
}

void DTCFastCommandInterface::SendGlobalCounterResetCalPulse(uint8_t pDuration)
{
    unsigned nullChanId = 999;
    SendCounterResetCalPulse(nullChanId);
}

void DTCFastCommandInterface::SendGlobalCustomFastCommands(std::vector<FastCommand>& pFastCmd)
{
    SendFastCommands(pFastCmd, 0);
}

void DTCFastCommandInterface::SendGlobalRepetitiveL1A(unsigned pNtriggers)
{
    unsigned nullChanId = 999;
    SendRepetitiveL1As(nullChanId, pNtriggers);
}

void DTCFastCommandInterface::SendReSync(unsigned pChanId)
{
    FastCommand cFastCommand;
    cFastCommand.resync_en = true;
    cFastCommand.bc0_en    = true;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, 0); }
    else { SendFastCommands(fastCmd, 0); }
}

void DTCFastCommandInterface::SendCalPulse(unsigned pChanId)
{
    FastCommand cFastCommand;
    cFastCommand.cal_pulse_en = true;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, 0); }
    else { SendFastCommands(fastCmd, 0); }
}

void DTCFastCommandInterface::SendL1A(unsigned pChanId)
{
    FastCommand cFastCommand;
    cFastCommand.l1a_en = true;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, 0); }
    else { SendFastCommands(fastCmd, 0); }
}

void DTCFastCommandInterface::SendCounterReset(unsigned pChanId)
{
    FastCommand cFastCommand;
    cFastCommand.bc0_en = true;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, 0); }
    else { SendFastCommands(fastCmd, 0); }
}

void DTCFastCommandInterface::SendCounterResetL1A(unsigned pChanId)
{
    FastCommand cFastCommand;
    cFastCommand.bc0_en = true;
    cFastCommand.l1a_en = true;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, 0); }
    else { SendFastCommands(fastCmd, 0); }
}

void DTCFastCommandInterface::SendCounterResetCalPulse(unsigned pChanId)
{
    FastCommand cFastCommand;
    cFastCommand.bc0_en       = true;
    cFastCommand.cal_pulse_en = true;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, 0); }
    else { SendFastCommands(fastCmd, 0); }
}

void DTCFastCommandInterface::SendRepetitiveL1As(unsigned pChanId, unsigned pNtriggers)
{
    FastCommand cFastCommand;
    cFastCommand.l1a_en  = true;
    cFastCommand.bx_wait = 400;
    std::vector<FastCommand> fastCmd{cFastCommand};

    if (pChanId != 999) { SendFastCommands(pChanId, fastCmd, pNtriggers); }
    else { SendFastCommands(fastCmd, pNtriggers); }
}

void DTCFastCommandInterface::SendFastCommands(std::vector<FastCommand>& pFastCmd, signed pRepeat)
{
    bool repeat = (pRepeat==0) ? false : true;
    unsigned nRepeat = (pRepeat<0) ? 0 : pRepeat;

    std::vector<unsigned> patterns;
    GeneratePattern(pFastCmd, patterns, repeat);

    GlobalResetPatternGen();
    GlobalRepeatFastCommand(nRepeat);
    GlobalLoadPatternGen(patterns);
    GlobalStartPatternGen();
}

void DTCFastCommandInterface::SendFastCommands(unsigned pChanId, std::vector<FastCommand>& pFastCmd, signed pRepeat)
{
    bool repeat = (pRepeat==0) ? false : true;
    unsigned nRepeat = (pRepeat<0) ? 0 : pRepeat;

    std::vector<unsigned> patterns;
    GeneratePattern(pFastCmd, patterns, repeat);

    LocalResetPatternGen(pChanId);
    LocalRepeatFastCommand(pChanId, nRepeat);
    LocalLoadPatternGen(pChanId, patterns);
    LocalStartPatternGen(pChanId);
}

void DTCFastCommandInterface::PrintGlobalCounters()
{
    uint32_t n_resync_en    = ReadReg("payload.tcds_fast_cmd.status.fast_resets");
    uint32_t n_l1a_en       = ReadReg("payload.tcds_fast_cmd.status.l1a_trigs");
    uint32_t n_cal_pulse_en = ReadReg("payload.tcds_fast_cmd.status.cal_pulses");
    uint32_t n_bc0_en       = ReadReg("payload.tcds_fast_cmd.status.counter_resets");

    LOG(INFO) << BOLDBLUE << "Fast commands sent from global TCDS :: FastReset " << n_resync_en << " :: L1A " << n_l1a_en
                          << " :: CalPulse " << n_cal_pulse_en << " :: CntReset " << n_bc0_en << RESET;
}

void DTCFastCommandInterface::PrintLocalCounters(unsigned pChanId)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    uint32_t n_resync_en    = ReadReg("payload.fe_chan.fast_cmd.status.fast_resets");
    uint32_t n_l1a_en       = ReadReg("payload.fe_chan.fast_cmd.status.l1a_trigs");
    uint32_t n_cal_pulse_en = ReadReg("payload.fe_chan.fast_cmd.status.cal_pulses");
    uint32_t n_bc0_en       = ReadReg("payload.fe_chan.fast_cmd.status.counter_resets");

    LOG(INFO) << BOLDBLUE << "Fast commands sent from local TCDS on channel " << pChanId << " :: FastReset " << n_resync_en << " :: L1A " << n_l1a_en
                          << " :: CalPulse " << n_cal_pulse_en << " :: CntReset " << n_bc0_en << RESET;
}

void DTCFastCommandInterface::SetFastCommandDelays(unsigned pChanId, unsigned pDelay)
{
    if(pDelay > 7)
    {
        pDelay = 7;
        LOG(WARNING) << BOLDRED << "Cannot delay per channel fast commands by more than 7 BX : Setting to 7 BX" << RESET;
    }

    //WriteReg("payload.fe.chan_sel", pChanId);
    //WriteReg("payload.fe_chan.fast_cmd.ctrl.delay", pDelay);

    std::vector<std::pair<std::string, uint32_t>> regWriteVec{
        std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChanId },             //select channel
        std::pair<std::string, uint32_t>{ "payload.fe_chan.fast_cmd.ctrl.delay", pDelay }
    };
    WriteStackReg(regWriteVec);
}

void DTCFastCommandInterface::GeneratePattern(std::vector<FastCommand>& pFastCmd, std::vector<unsigned>& pPatterns, bool repeat)
{
    for(auto fast_cmd: pFastCmd)
    {
     	if(fast_cmd.bx_wait > 1023)
        {
            fast_cmd.bx_wait = 1023;
            LOG(WARNING) << BOLDRED << "Cannot wait for more than 1023 BX after sending a fast command : Setting to 1023 BX" << RESET;
        }
	// load fast command pattern - by default the generator is instructed to move to the next command word
        pPatterns.push_back((1 << 14) | (fast_cmd.bx_wait << 4) | (fast_cmd.resync_en << 3) | (fast_cmd.l1a_en << 2) | (fast_cmd.cal_pulse_en << 1) | fast_cmd.bc0_en);

    }

    // modify last command word to instruct generator to stop, or repeat sequence from the first word
    if(!pPatterns.empty())
    {
     	unsigned last_cmd = pPatterns.back() & 0x3fff;
        if(!repeat) last_cmd = last_cmd | (2 << 14);
        pPatterns.back() = last_cmd;
    }
}

void DTCFastCommandInterface::GlobalSourceSelect(unsigned pSource)
{
    if(pSource > 15) { LOG(ERROR) << BOLDRED << "Invalid Fast Command local source select option" << RESET; }
    else
    {
        WriteReg("payload.tcds_fast_cmd.ctrl.source_mask", pSource);
    }
}

void DTCFastCommandInterface::GlobalRepeatFastCommand(unsigned pRepeat)
{
    if(pRepeat > 1023)
    {
        pRepeat = 1023;
        LOG(WARNING) << BOLDRED << "Cannot repeat fast command pattern more than 1023 times : Setting to 1023" << RESET;
    };
    WriteReg("payload.tcds_fast_cmd.ctrl.repeat", pRepeat);
}

void DTCFastCommandInterface::GlobalStartPatternGen()
{
    WriteReg("payload.tcds_fast_cmd.ctrl.run", 0x1);
}

void DTCFastCommandInterface::GlobalResetPatternGen()
{
    WriteReg("payload.tcds_fast_cmd.ctrl.run", 0x0);
}

void DTCFastCommandInterface::GlobalLoadPatternGen(std::vector<unsigned>& pPatterns)
{
    int i = 0;
    for(auto pattern: pPatterns)
    {
        i++;
        std::string reg_name = "payload.tcds_fast_cmd.pgen_buf_" + std::to_string(i);
        if(i <= 6) WriteReg(reg_name, pattern);
    }
}

void DTCFastCommandInterface::LocalSourceSelect(unsigned pChanId, unsigned pSource)
{
    //WriteReg("payload.fe.chan_sel", pChanId);
    if(pSource > 15) {
        LOG(ERROR) << BOLDRED << "Invalid Fast Command local source select option" << RESET; 
    }
    else
    {
        //WriteReg("payload.fe_chan.fast_cmd.ctrl.source_mask", pSource);
        std::vector<std::pair<std::string, uint32_t>> regWriteVec{
            std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChanId },             //select channel
            std::pair<std::string, uint32_t>{ "payload.fe_chan.fast_cmd.ctrl.source_mask", pSource }
        };
        WriteStackReg(regWriteVec);
    }
}

void DTCFastCommandInterface::LocalRepeatFastCommand(unsigned pChanId, unsigned pRepeat)
{
    if(pRepeat > 1023)
    {
        pRepeat = 1023;
        LOG(WARNING) << BOLDRED << "Cannot repeat fast command pattern more than 1023 times : Setting to 1023" << RESET;
    }
    //WriteReg("payload.fe.chan_sel", pChanId);
    //WriteReg("payload.fe_chan.fast_cmd.ctrl.repeat", pRepeat);
    std::vector<std::pair<std::string, uint32_t>> regWriteVec{
        std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChanId },             //select channel
        std::pair<std::string, uint32_t>{ "payload.fe_chan.fast_cmd.ctrl.repeat", pRepeat }
    };
    WriteStackReg(regWriteVec);
}

void DTCFastCommandInterface::LocalStartPatternGen(unsigned pChanId)
{
    //WriteReg("payload.fe.chan_sel", pChanId);
    //WriteReg("payload.fe_chan.fast_cmd.ctrl.run", 0x1);
    std::vector<std::pair<std::string, uint32_t>> regWriteVec{
        std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChanId },             //select channel
        std::pair<std::string, uint32_t>{ "payload.fe_chan.fast_cmd.ctrl.run", 0x1 }
    };
    WriteStackReg(regWriteVec);
}

void DTCFastCommandInterface::LocalResetPatternGen(unsigned pChanId)
{
    //WriteReg("payload.fe.chan_sel", pChanId);
    //WriteReg("payload.fe_chan.fast_cmd.ctrl.run", 0x0);
    std::vector<std::pair<std::string, uint32_t>> regWriteVec{
        std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChanId },             //select channel
        std::pair<std::string, uint32_t>{ "payload.fe_chan.fast_cmd.ctrl.run", 0x0 }
    };
    WriteStackReg(regWriteVec);
}

void DTCFastCommandInterface::LocalLoadPatternGen(unsigned pChanId, std::vector<unsigned>& pPatterns)
{
    //WriteReg("payload.fe.chan_sel", pChanId);
    std::vector<std::pair<std::string, uint32_t>> regWriteVec{
        std::pair<std::string, uint32_t>{ "payload.fe.chan_sel", pChanId }              //select channel
    };
    int i = 0;
    std::pair<std::string, uint32_t> regWritePair;
    for(auto pattern: pPatterns)
    {
     	i++;
	    std::string reg_name = "payload.fe_chan.fast_cmd.pgen_buf_" + std::to_string(i);
        if(i <= 6) {
            regWritePair = std::pair<std::string, uint32_t>{ reg_name, pattern };
            regWriteVec.push_back(regWritePair);
            //WriteReg(reg_name, pattern);
        }
    }
}

} // namespace Ph2_HwInterface
#endif
