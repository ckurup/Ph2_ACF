

# Stage 1: Set starting imag
FROM  gitlab-registry.cern.ch/ckurup/ph2_acf:base

# Use bash shell
SHELL ["/bin/bash", "-c"]

# Copy the entire repository to /app in the container
COPY . /Ph2_ACF

# Set the working directory to /app
WORKDIR /Ph2_ACF

# Ensure scripts have executable permissions
RUN source setup.sh && mkdir -p build && cd build && cmake .. && make -j$(nproc)
